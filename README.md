# RIZ_SNR_Computations


a notebook using the ETC from may2021 to analyse SNR performances for RIZ / UBV

# Preliminary detector selection and coatings
U: CCD290-99, standard silicon, astro broad-band-2, (CCD290-99-0-F82), 350 - 400nm
B: CCD290-99, standard silicon, astro broad-band-2, (CCD290-99-0-F82), 400 - 500nm
V: CCD290-99, standard silicon, astro broad-band-2, (CCD290-99-0-F82), 490 - 630nm
R: CCD290-99, deep depleted, astro broad-band-2, (CCD290-99-0-F24), 620 - 760nm
Iz: CCD290-99, deep depleted, astro broad-band-2, (CCD290-99-0-F24), 750 - 950nm