#!/usr/bin/env python3
"""Module providing SNR Computations fro ANDES"""
from dataclasses import dataclass
import numpy as np
from scipy.interpolate import interp2d

import pandas as pd

@dataclass(repr=True)
class EM:
    """
    Class that models the Exposuremeter of UBVRIZ
    Light is taken out of F2F interface, so does not go through the spectrographs
    """
    def __init__(self,WL = np.array([0.350,0.550,0.620,0.630,0.850,0.950])):
        dataclass.__init__(self)
        self.WL = WL
        # efficiency of the F2F interface and other fiber couplings
        self.f2feff = 0.8 * np.ones_like(self.WL)
        # efficiency of the internal EM optics
        self.emeff = 0.8 * np.ones_like(self.WL)
#        self.EFF_data_table = np.loadtxt("ANDES_UBV_transmission_all.dat")
        self.EFF = self.f2feff * self.emeff

    def get_efficiency(self):
        return self.EFF


@dataclass(repr=True)
class RIZ:
    """
    Class that model the RIZ spectrograph in detail for throughput :
    It tries to take into account all the surfaces of the spectrograph,
    the quantum efficiency of the detector and the diffraction efficiency of he grating.
    No attempt to better compute the number of pixel per resolution element has been made
    """
    alpha = np.deg2rad(76.)
    blaze = np.deg2rad(76.)
    gpmm = 0.0316         # line/µm
    m_ge=-0.5*(0.0231+0.0217) /100.*1000. # percentage/nm -> fraction per µm
    p_ge=0.5*(81.692+83.736) /100.  # percentage -> fraction
    min_throu = 0.15
    max_throu = 0.30
    dec_spacing = 2
    ncam = 1
    n_surfopt: int = 27
    coat_alu: float = 0.975
    coat_m: float = 0.99
    AR_nb: float = 0.99
    AR_L35: float = 0.9675
    AR_L6: float = 0.985
    AR_BS: float =0.987
    R_BS : float = 0.99
    vign_EG : float = 0.985909
    Eff_CD : float = 0.8
    AR_prism: float = 0.94
    QE_data: str = "E2V_astro_multi2.dat"
#    WL : np.array = np.array([0.550,0.850,0.950])
    ordermin: int = 62
    ordermax: int = 98
    order_inter: int = 81
    fR : float = 483 # to be checked
    fIZ : float = 400 # to be checked
    detector_width: float = 90 # mm

    def __init__(self,WL = np.array([0.550,0.850,0.950]),QE_data = "E2V_astro_multi2.dat"):
        dataclass.__init__(self)
        self.WL = WL
        self.QE_data = QE_data
        self.surf_throughtput = np.zeros((self.n_surfopt))
        self.surf_throughtput[0:4]= [self.AR_nb, self.AR_L35, self.AR_nb, self.AR_nb] # anamorphic lenses
        self.surf_throughtput[4]= self.coat_alu*self.coat_alu # M1 coll
        self.surf_throughtput[5]= self.vign_EG # vignetting EG
        self.surf_throughtput[6]= self.coat_m # FM1
        self.surf_throughtput[7:9]= [self.AR_nb, self.AR_nb] # FL
        self.surf_throughtput[9:11]= [self.coat_m, self.coat_m] # FM2 & CollM2
        self.surf_throughtput[11:13]= [self.AR_BS, self.AR_BS] # BS
        self.surf_throughtput[13:15]= [self.AR_prism, self.Eff_CD] # prism CD
        self.surf_throughtput[15:17]= [self.AR_nb, self.AR_nb] # L1 
        self.surf_throughtput[17:19]= [self.AR_nb, self.AR_nb] # L2 
        self.surf_throughtput[19:21]= [self.AR_L35, self.AR_L35] # L3 
        self.surf_throughtput[21:23]= [self.AR_nb, self.AR_nb] # L4 
        self.surf_throughtput[23:25]= [self.AR_L35, self.AR_L35] # L5 
        self.surf_throughtput[25:27]= [self.AR_L6, self.AR_L6] # L6 
        self.grating_eff = (self.m_ge*self.WL+self.p_ge)
        self.QE_data_table = np.loadtxt(self.QE_data)
        self.QE = np.interp(self.WL,self.QE_data_table[:,0]/1000,self.QE_data_table[:,1])/100
        self.PDE_data_table = np.loadtxt("RIZ_pure_diffraction_efficiency.dat")
        self.PDE = np.interp(self.WL,self.PDE_data_table[:,0],self.PDE_data_table[:,1])

    def get_efficiency(self):
        self.EFF_RIZ = np.product(self.surf_throughtput) * self.grating_eff * self.QE * self.PDE
        return np.product(self.surf_throughtput) * self.grating_eff * self.QE * self.PDE

    
@dataclass(repr=True)
class UBV:
    """
    Class that model the UBV spectrograph in detail for throughput :
    It tries to take into account all the surfaces of the spectrograph,
    the quantum efficiency of the detector and the diffraction efficiency of he grating.
    No attempt to better compute the number of pixel per resolution element has been made
    """
    alpha = np.deg2rad(76.)
    blaze = np.deg2rad(76.)
    gpmm = 0.04156         # line/µm
    m_ge=-0.5*(0.0231+0.0217) /100.*1000. # percentage/nm -> fraction per µm
    p_ge=0.5*(81.692+83.736) /100.  # percentage -> fraction
    min_throu = 0.15
    max_throu = 0.30
    dec_spacing = 2
    ordermin: int = 74
    ordermax: int = 133
    order_inter: int = 81
    fR : float = 483 # to be checked
    fIZ : float = 400 # to be checked
    detector_width: float = 90 # mm
    
    def __init__(self,WL = np.array([0.350,0.550,0.620])):
        dataclass.__init__(self)
        self.WL = WL
        self.EFF_data_table = np.loadtxt("ANDES_UBV_transmission_all.dat")
        self.EFF = np.interp(self.WL,self.EFF_data_table[:,0],self.EFF_data_table[:,1])

    def get_efficiency(self):
        return self.EFF
    

@dataclass(repr=True)
class Instrument:
    """
    Class that model the transmission of the other element of the instrument than the spectrograph
    Frontend and Fiber link
    """
    throughput_file: str = "ANDES Transmission .xlsx"
#    WL: np.array = np.array([0.620,0.850,0.950])
    sheet_name_FE: str = "FE RIZ"
    sheet_name_FL: str = "FL RIZ"
    
    def __init__(self,WL = np.array([0.550,0.850,0.950]), sheet_name_FE = "FE RIZ", sheet_name_FL = "FL RIZ", **kwargs):
        dataclass.__init__(self)
        self.WL = WL
        self.sheet_name_FE = sheet_name_FE
        self.sheet_name_FL = sheet_name_FL
        self.FE_table = pd.read_excel(self.throughput_file,sheet_name = self.sheet_name_FE,engine='openpyxl')
        self.FL_table = pd.read_excel(self.throughput_file,sheet_name = self.sheet_name_FL,engine='openpyxl')
        self.FE = np.interp(self.WL,self.FE_table["Lambda (nm)"]/1000,self.FE_table["Transmission"])
        self.FL = np.interp(self.WL,self.FL_table["Lambda (nm)"]/1000,self.FL_table["Transmission"])
        if "l1" in kwargs.keys():
            self.fake_dichro_FE(kwargs["l1"],kwargs["l2"],kwargs["w1"],kwargs["w2"],kwargs["max_T"])
       

    def fake_dichro_FE(self,l1,l2,w1,w2,max_T):
        self.FE = np.ones(self.WL.shape)*max_T
        self.FE[self.WL < (l1-w1/2)] = 0
        self.FE[self.WL > (l2+w2/2)] = 0
        self.FE[(self.WL >= (l1-w1/2)) & (self.WL <= (l1+w1/2))] = max_T*(1+np.sin((self.WL[(self.WL >= (l1-w1/2)) & (self.WL <= (l1+w1/2))]-l1)*np.pi/2/(w1/2)))/2
        self.FE[(self.WL >= (l2-w2/2)) & (self.WL <= (l2+w2/2))] = max_T*(1-np.sin((self.WL[(self.WL >= (l2-w2/2)) & (self.WL <= (l2+w2/2))]-l2)*np.pi/2/(w2/2)))/2        
        




@dataclass(repr=True)
class ANDES:
    """Dataclass for ANDES"""
    # Tabular values for sky-backgroung
    TAB_SKY_WL = np.array([0.36, 0.44, 0.55, 0.64, 0.80, 1.05, 1.25, 1.65, 2.60])
    TAB_SKY_MAG = np.array([22.5, 22.5, 21.8, 21.5, 20.5, 20.5, 20.0, 19.5, 19.5])
    # Tabular values for alpha=e^-(tau(lambda)) (ESO)
    TAB_ALPHA_WL = np.array([0.35, 0.365, 0.44, 0.55, 0.70, 0.90, 1.00, 1.25, 1.5, 2.0, 2.2, 2.4, 2.6])
    TAB_ALPHA_VAL_0 = np.array([64.0,66.95,82.67,90.36,98.08,99.86,100.0,100.0,100.0,100.0,100.0,100.0,100.0,])
    # Tabular values for telescope efficiency (ESO)
    TAB_EFFT_WL = np.linspace(0.35, 2.60, 46)
    TAB_EFFT_VAL_0 = np.array([0.10,0.299,0.469,0.565,0.61,0.642,0.674,0.699,0.712,0.713,0.715,0.757,0.788,0.803,0.812,0.82,0.826,0.834,0.842,0.848,0.855,0.86,0.865,0.87,0.873,0.876,0.878,0.88,0.88,0.881,0.881,0.88,0.882,0.882,0.883,0.883,0.884,0.884,0.884,0.884,0.884,0.885,0.885,0.886,0.886,0.886,])
    # Tabular values for instrument efficiency (ANDES!)
    TAB_EFFI_WL = np.array([0.36, 0.44, 0.55, 0.64, 0.80, 1.05, 1.25, 1.65, 2.60])
    TAB_EFFI_VAL = np.array([1.0, 1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0 ])
    # Tabular values for encircled energy (ESO)
    TAB_EE_WL = np.array([0.36, 0.44, 0.55, 0.64, 0.80, 1.25, 1.65, 2.16, 3.50])
    TAB_EE_00044 = np.array([0.00229, 0.00263, 0.00282, 0.00325, 0.00380, 0.00480, 0.00474, 0.00646, 0.0142])
    TAB_EE_002 = np.array([0.0478, 0.0523, 0.0584, 0.0685, 0.0792, 0.0968, 0.116, 0.153, 0.290])
    TAB_EE_01 = np.array([1.19, 1.30, 1.46, 1.67, 1.94, 2.42, 2.93, 3.76, 6.44])
    TAB_EE_02 = np.array([4.66, 5.08, 5.68, 6.47, 7.46, 9.13, 11.0, 13.7, 20.2])
    TAB_EE_04 = np.array([17.1, 18.4, 20.3, 22.7, 25.6, 30.1, 34.7, 40.1, 49.8])
    TAB_EE_06 = np.array([33.5, 35.6, 38.5, 42.0, 46.1, 51.8, 56.8, 61.9, 69.5])
    TAB_EE_08 = np.array([50.0, 52.4, 55.5, 59.1, 63.0, 68.0, 71.9, 75.6, 80.5])
    TAB_EE_1 = np.array([64.0, 66.1, 68.8, 71.8, 74.8, 78.5, 81.2, 83.6, 86.7])
    TAB_EE_16 = np.array([88.0, 88.3, 88.9, 89.8, 90.7, 91.8, 92.6, 93.3, 94.2])
    TAB_EE_DAPE = np.array([0.0044, 0.02, 0.1, 0.2, 0.4, 0.6, 0.8, 1.0, 1.6])
    # Fixed values
    FCAM : float = 1.5 # F ratio of camera (used to compute the number of pixel per resolution element)
    TBCK : float = 283.0 # Background temperature
    EBCK : float= 0.2 # Emissivity of the sky background
    DTEL : float= 38.5 # Diameter of telescope
    COBS : float= 0.28 # Central Obscuration
    RPOW : float= 1e5 # Resolution power
    DAPE : float= 0.87 # Diameter of the telescope
    # Semi-fixed
    NDIT : float= 1 # Number of separate readouts
    DARKCUR : float = 0.1 # Dark Current
    PIXBINNED : float = 1 # Binning factor
    DPIX: float = 10.0e-6 # Detector pixel size in m
    RON: float = 1.0 # Readout noise of the detector
    # Inputs
    EXPTIME: float = 1   # Exposure Time
    WL : float = -1 # Wavelength
    SN : float = -1 # Signal to Noise
    VMAG : float = -50 # Magnitude of the source (AB magnitude)
    SBMAG : float= -50 # Surface Brightness Magnitude
    AM : float = 1 # Airmass
    # Derived from above
    ATEL : float =  np.pi / 4.0 * 10**4 * DTEL**2 * ( 1.0 - COBS**2 ) # area of the telescope cm^2
    XANPIX : float = 0.036 / ( FCAM / 1.5 ) * ( DPIX / 1e-6 ) / ( DTEL / 38.5 )
    YANPIX : float = XANPIX
    PIXAPE : float = np.pi / 4.0 * DAPE**2 / XANPIX**2
    TOTPIXRE : float = np.ceil( PIXAPE / PIXBINNED ) # Total number of pixels per resolution element
    VNOISEDET : float = RON # Detector Read out Noise ?
    # Intermediaries and outputs
    SKYBCKAM : float = 0
    SIGMASKY : float = 0
    BBEXP : float = 0
    SIGMATH : float = 0
    EFFTEL : float = 0 # Efficiency of the telescope
    ALPHAP : float = 0
    AT : float = 0
    EFFTELTOT : float = 0 # Efficiency of the telescope
    EFF : float = 0 # Total efficiency
    VNBCK : float = 0
    VNOISEBCK : float = 0 # Background Noise of detector
    SKYBCK : float = -1 # Sky Background
    SLE : float = -1 #
    EFF_FE : float = 0.7
    EFF_FL : float = 0.4
    EFF_SPE : float = -1
    EFFI : float = 0.08 # Efficiency of the instrument
    EFFT : float = -1 # Efficiency of the telescope ?
    ALPHA : float = -1
    NOISETOT : float = -1
    NOBJ : float = -1
    NBCK : float = -1
    QE_data : str = "E2V_astro_multi2.dat"
    SPECTRO : str = "RIZ"

    def GetIntermediaries(self):
        """Calculate intermediary values dependendent on inputs"""
        self.WL = np.array(self.WL)
        #print(self.WL)
        # set slit efficiency to 1
        self.SLE = 1.0
        # Efficiency is Telescope * Atmospheric Transmission * Instrument Efficiency
        self.EFF = np.interp( self.WL , self.TAB_EFFT_WL , self.TAB_EFFT_VAL_0 ) * 0.95 * \
                          np.interp( self.WL , self.TAB_ALPHA_WL , self.TAB_ALPHA_VAL_0 / 100 )**self.AM * \
                          np.interp( self.WL , self.TAB_EFFI_WL , self.TAB_EFFI_VAL )
        #print(f"EFFTEL : {np.interp( self.WL , self.TAB_EFFT_WL , self.TAB_EFFT_VAL_0 )*0.95}")
        # Sky background at WL, at AM
        
        #instru = Instrument(self.WL, self.SPECTRO)
        if self.SPECTRO == "RIZ":
            instru = Instrument(self.WL, sheet_name_FE = "FE RIZ", sheet_name_FL = "FL RIZ", l1=0.628, l2=0.99, w1=0.05, w2=0.0001, max_T=0.7)
            self.EFF_FE = instru.FE
            self.EFF_FL = instru.FL
            riz = RIZ(self.WL,self.QE_data)
            self.EFFI = riz.get_efficiency()*self.EFF_FE * self.EFF_FL
        
        if self.SPECTRO == "UBV":
            instru = Instrument(self.WL, sheet_name_FE = "FE UBV", sheet_name_FL = "FL UBV", l1=0.34, l2=0.628, w1=0.0001, w2=0.05, max_T=0.7)
            self.EFF_FE = instru.FE
            self.EFF_FL = instru.FL
            ubv = UBV(self.WL)
            self.EFFI = ubv.get_efficiency()*self.EFF_FE * self.EFF_FL

        if self.SPECTRO == "EM":
            instru = Instrument(self.WL, sheet_name_FE = "FE UBV", sheet_name_FL = "FL UBV", l1=0.34, l2=0.628, w1=0.0001, w2=0.05, max_T=0.7)
            self.EFF_FE = instru.FE
            self.EFF_FL = instru.FL
            em = EM(self.WL)
            self.EFFI = em.get_efficiency() * self.EFF_FE * self.EFF_FL
            
        self.SKYBCK = np.interp( self.WL , self.TAB_SKY_WL , self.TAB_SKY_MAG )
        self.SKYBCKAM = self.SKYBCK - 0.4 * ( self.AM - 1.0 )
        # Background flux in aperture per resolution element
        self.SIGMASKY = 10**( ( 16.85 - self.SKYBCKAM)/2.5 ) / self.RPOW # ph cm-2 s-1 arcsec-2
        BBEXP = -14388 / ( self.WL * self.TBCK )
        BBEXP[BBEXP<-40] = -40
        self.SIGMATH = ( 1.4 * 10**12 * self.EBCK * np.e**(BBEXP ) ) / ( self.WL**3 * self.RPOW )
        self.VNBCK = self.EFF * self.ATEL * np.pi / 4.0 * self.DAPE**2 * ( self.SIGMASKY + self.SIGMATH )
        self.VNOISEBCK = np.sqrt( self.VNBCK * self.EXPTIME )
        
        self.XANPIX = self.DPIX * 1e-6 / self.DTEL / self.FCAM * 3600 * 180 / 3.141593  # arcsec
        self.YANPIX = self.XANPIX  # arcsec
        self.PIXAPE = 0.7854 * self.DAPE**2 / (self.XANPIX * self.YANPIX)  # pixels
        self.TOTPIXRE = int(self.PIXAPE / self.PIXBINNED)
        if self.PIXBINNED > int(self.PIXAPE):
            print("PIXBINNED cannot be larger than the total number of pixels per resolution element")
        
        self.VNOISEDET = np.sqrt( self.PIXAPE * (float(self.NDIT) * self.RON**2 / self.PIXBINNED + self.DARKCUR * self.EXPTIME/3600))  # electrons
        
    def ComputeSlitEfficiency(self,):
        """
        Compute Slit Efficiency
        """
        self.EEtable = np.c_[self.TAB_EE_00044,self.TAB_EE_002, self.TAB_EE_01, self.TAB_EE_02, self.TAB_EE_04, self.TAB_EE_06, self.TAB_EE_08, self.TAB_EE_1, self.TAB_EE_16].T
        EEtable=interp2d(self.TAB_EE_WL,self.TAB_EE_DAPE,self.EEtable)
        self.SLE = EEtable(self.WL,self.DAPE)/100.
        return self.SLE
        
        
    def GetPhotons(self):
        """Return number of photons at the end of Fiber Link"""
        self.GetIntermediaries()
        self.ComputeSlitEfficiency()
        self.NOBJ = self.SLE * self.EFF * self.EFF_FE * self.EFF_FL * self.ATEL * self.EXPTIME / self.RPOW * 10**( ( 16.85 - self.VMAG ) / 2.5 )
        return self.NOBJ
    
    def GetSkyBackgroundPhotons(self):
        """Return number of photons of the sky at the end of Fiber Link"""
        self.GetIntermediaries()
        self.ComputeSlitEfficiency()
        self.NBCK = self.SLE * self.EFF * self.EFF_FE * self.EFF_FL * self.ATEL * self.EXPTIME * self.SIGMASKY * np.pi * self.DAPE**2 / 4
        return self.NBCK
    
    
    
    def GetSN(self):
        """Return calculated SN for given exposure time and magnitude"""
        self.GetIntermediaries()
        self.ComputeSlitEfficiency()
        self.SN = np.sqrt(
            ( self.SLE * self.EFF* self.ATEL * self.EXPTIME / self.RPOW * 10**( ( 16.85 - self.VMAG ) / 2.5 ) )**2
            /
            ( self.VNOISEBCK**2 + self.VNOISEDET**2 + self.SLE * self.EFF * self.ATEL * self.EXPTIME / self.RPOW * 10**( ( 16.85 - self.VMAG ) / 2.5 ) )
        )
        return self.SN
    
    def GetSNSpectro(self):
        """Return calculated SN for given exposure time and magnitude on the spectrograph"""
        self.GetIntermediaries()
        self.ComputeSlitEfficiency()
        self.SN = np.sqrt(
            ( self.SLE * self.EFF* self.EFFI * self.ATEL * self.EXPTIME / self.RPOW * 10**( ( 16.85 - self.VMAG ) / 2.5 ) )**2
            /
            ( self.EFFI*(self.VNOISEBCK)**2 + self.VNOISEDET**2 + self.SLE * self.EFF * self.EFFI *self.ATEL * self.EXPTIME / self.RPOW * 10**( ( 16.85 - self.VMAG ) / 2.5 ) )
        )
        return self.SN
    

    def GetMag(self):
        """Return calculated limiting magnitude for given exposure time and S/N"""
        self.GetIntermediaries()
        return 43.0

    def GetSBMag(self):
        """Return calculated limiting magnitude surface brightness for given exposure time and S/N"""
        self.GetIntermediaries()
        return 44.0

if __name__ == "__main__":
    test = ANDES()
    test.VMAG = 20.0
    test.EXPTIME = 42.0
    test.WL = 0.666
    print(test)
    print(test.GetPhotons())
    print(f"For ExpT {test.EXPTIME} and Mag {test.VMAG} at WL {test.WL} we get SN {test.GetSN()}")
    print(test.GetMag())
    print(test.GetSBMag())
