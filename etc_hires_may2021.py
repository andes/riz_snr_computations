#!/usr/local/bin/python3


import argparse
import numpy as np
from numpy import sqrt, log10, exp
from sys import exit, argv


# On interpolate EFFT, last index is 49... for goes only to 46
# for display results in SB, there is no SLE output (also not computed)
# for SN, check parameters do not include EFFT in the original fortran


def compareFloat(a, b):
    """
    Function used to compare floating points to a small precision
       Parameters:
                a (float): some float number
                b (float): another float number

        Returns:
                result (bool): True if a == b false otherwise
    """

    result = np.abs(a - b) < 1e-16
    return result


def interpolateOnTable(valueIn, WL, tableWL, tableV, weight=1.0):
    """
    Function to interpolate a value on a genetic given table
       Parameters:
                valueIn (float) : input value to be interpolated
                WL      (float) : WL to be used in the interpolation
                tableWL ([float]) : WL table
                tableV  ([float]) : Value table
                weight  (float) : value weight (for the case of EFFT)
        Returns:
                valueOut (float): interpolated value
    """

    #
    # Interpolate value on table
    #

    valueOut = valueIn
    if valueIn < 0:
        if WL <= tableWL[0]:
            valueOut = tableV[0] * weight
            return valueOut
        if WL > tableWL[-1]:
            valueOut = tableV[-1] * weight
            return valueOut
        itab = 1

        for i in range(1, len(tableWL)):
            if WL < tableWL[i]:
                y2 = tableV[i] * weight
                y1 = tableV[i - 1] * weight
                x2 = tableWL[i]
                x1 = tableWL[i - 1]
                valueOut = y1 + (y2 - y1) * (WL - x1) / (x2 - x1)
                return valueOut

    return valueOut


def validadeSNParameter():
    """
    Function to validate SN variable
       Parameters:
                    None (SN comes from a global variable)
        Returns:
              None
    """

    if SN <= 0.0:
        print("Must specify value of S/N ratio, SN=.. \n")
        exit(1)


def validadeVMAGParameter():
    """
    Function to validate VMAG variable
       Parameters:
                    None (VMAG comes from a global variable)
        Returns:
              None
    """

    if VMAG < -48.0:
        print("Must specify value of mag, MA=.. \n")
        exit(1)


def validadeSBMAGParameter():
    """
    Function to validade SBMAG variable
       Parameters:
                    None (SBMAG comes from a global variable)
        Returns:
              None
    """

    if SBMAG <= -49.0:
        print("Must specify value of surface brightness, SB=.. \n")
        exit(1)


def validadeGeneralParameters():
    """
    Function to validate SN variable
       Parameters:
                    None (validation happens to all global variable)
        Returns:
              None
    """

    #
    # Check parameters
    #
    errorMsg = ""
    if WL <= 0.0:
        errorMsg += "Must specify a value for wavelength, WL=.. \n"
    if EXPTIMES <= 0.0:
        errorMsg += "Must specify a value for exposure time, EX=.. \n"
    if WL < 0.35 or WL > 2.5:
        errorMsg += "WL out of range (min 0.36, max 2.5) \n"
    if NDIT <= 0:
        errorMsg += "NDIT must be >= zero \n"
    if RPOW < 1 or RPOW > 3e5:
        errorMsg += "RPOW must be >100 and <300,000 \n"
    if EFFI <= 0 or EFFI > 1.0:
        errorMsg += "EFFI must be >0 and <1 \n"
    if EFFT > 1.0:
        errorMsg += "EFFT must be >0 and <1 \n"
    if DAPE <= -2 or DAPE > 3.0:
        errorMsg += "DAPE must be >0 and <3 \n"
    if TBCK < 200 or TBCK > 400:
        errorMsg += "TBCK must be >200 K and <400 K \n"
    if EBCK <= 0 or EBCK > 1:
        errorMsg += "EBCK must be >0 and <1 \n"
    if DTEL <= 0 or DTEL > 100:
        errorMsg += "DTEL must be >0 and <100 \n"
    if COBS <= 0 or COBS > 0.9:
        errorMsg += "COBS must be >0 and <0.9 \n"
    if DPIX <= -2 or DPIX > 1000:
        errorMsg += "DPIX must be >0 and <1000 microns \n"
    if FCAM <= 0.8 or FCAM > 1000:
        errorMsg += "FCAM must be >0.8 and <1000 \n"
    if RON < -2 or RON > 1000:
        errorMsg += "RON must be >0 and <1000 \n"
    if DARKCUR < -2 or DARKCUR > 1e9:
        errorMsg += "DARKCUR must be >0 and <1e6 \n"
    if AM < 0 or AM > 3.0:
        errorMsg += "AM must be >0 and <3 \n"
    if PIXBINNED < 1.0:
        errorMsg += "PIXBINNED cannot be smaller than 1 \n"
    if PIXBINNED > 1.0 and WL > 0.95:
        errorMsg += "on-chip binning is only possible at wl<0.95 mu (CCDs) \n"
    if errorMsg != "":
        print(errorMsg)
        exit(1)


def redefineParameters():
    """
    Function to set saturation and/or limits to global variables
       Parameters:
                    None (variables comes from the global scope)
        Returns:
              None
    """

    global DAPE
    global DPIX
    global RON
    global DARKCUR
    global PIXBINNED
    #
    # DAPE definition
    #
    if DAPE <= 0:
        if WL <= 0.95:
            DAPE = 0.87

    if WL > 0.95:
        DAPE = 0.71

    #
    # DPIX definition
    #

    if DPIX <= 0:
        if WL <= 0.95:
            DPIX = 10

    if WL > 0.95:
        DPIX = 15

    #
    # RON definition
    #

    if RON <= 0:
        if WL <= 0.95:
            RON = 1

    if WL > 0.95:
        RON = 3

    #
    # DARK definition
    #

    if DARKCUR <= 0:
        if WL <= 0.95:
            DARKCUR = 1

    if WL > 0.95:
        DARKCUR = 4

    #
    # PIXBINNED definition
    #

    if WL > 0.95:
        PIXBINNED = 1


def interpolateOnSLETable():
    """
    Function to interpolate on SL considering all values of TAB_EE_WL
       Parameters:
                    None (variables comes from the global scope)
        Returns:
              None
    """

    #
    # Interpolate on SLE table, if necessary
    #

    def generateSLE_WLValues():
        global SLE
        # generate TAB_EE for each table and normalize
        TAB_EE_P = [
            x
            for x in [
                TAB_EE_00044 / 100.0,
                TAB_EE_002 / 100.0,
                TAB_EE_01 / 100.0,
                TAB_EE_02 / 100.0,
                TAB_EE_04 / 100.0,
                TAB_EE_06 / 100.0,
                TAB_EE_08 / 100.0,
                TAB_EE_1 / 100.0,
                TAB_EE_16 / 100.0,
            ]
        ]

        # saturate under
        if SLE < 0:
            if WL <= TAB_EE_WL[0]:
                SLE_WL[:] = [x[0] for x in TAB_EE_P]
                return
        # saturate over

        if WL > TAB_EE_WL[-1]:
            SLE_WL[:] = [x[-1] for x in TAB_EE_P]

            return
        
        # interpolate

        jjtab = 1

        for jj in range(1, 8 + 1):
            if WL < TAB_EE_WL[jj]:
                x2q = TAB_EE_WL[jj]
                x1q = TAB_EE_WL[jj - 1]
                y2p = np.array([TAB_EE_P[i][jj] for i in range(9)])
                y1p = np.array([TAB_EE_P[i][jj - 1] for i in range(9)])
                SLE_P = y1p + (y2p - y1p) * (WL - x1q) / (x2q - x1q)
                SLE_WL[:] = SLE_P
                return

    global SLE
    # generate all SLE_WL values
    generateSLE_WLValues()
    # saturate under
    if DAPE <= TAB_EE_DAPE[0]:
        SLE = TAB_EE_DAPE[0]
        return

    # saturate over

    if DAPE > TAB_EE_DAPE[8]:
        SLE = TAB_EE_DAPE[8]

        return

    # interpolate

    ktab = 1

    for k in range(1, 8 + 1):
        if DAPE < TAB_EE_DAPE[k]:
            y2w1 = SLE_WL[k]
            y1w1 = SLE_WL[k - 1]
            x2w = TAB_EE_DAPE[k]
            x1w = TAB_EE_DAPE[k - 1]
            SLE = y1w1 + (y2w1 - y1w1) * (DAPE - x1w) / (x2w - x1w)
            return


def computeAuxiliaryValues():
    """
    Function to compute some values that are common to either SN, VMAG and SBMAG computations
       Parameters:
                    None (variables comes from the global scope)
        Returns:
              None
    """

    global EXPTIME
    global ATEL
    global XANPIX
    global YANPIX
    global PIXAPE
    global TOTPIXRE
    global VNOISEDET
    global SKYBCKAM
    global SIGMASKY
    global BBEXP
    global SIGMATH
    global EFFTEL
    global ALPHAP
    global AT
    global EFFTELTOT
    global EFF
    global VNBCK
    global VNOISEBCK

    EXPTIME = EXPTIMES / 3600.0
    ATEL = 7854 * (DTEL * DTEL) * (1 - (COBS * COBS))  # cm^2   7854=3.141593/4*10000
    XANPIX = DPIX * 1e-6 / DTEL / FCAM * 3600 * 180 / 3.141593  # arcsec
    YANPIX = XANPIX  # arcsec
    PIXAPE = 0.7854 * (DAPE * DAPE) / (XANPIX * YANPIX)  # pixels
    TOTPIXRE = int(PIXAPE / PIXBINNED)
    if PIXBINNED > int(PIXAPE):
        print(
            "PIXBINNED cannot be larger than the total number of pixels per resolution element"
        )
        exit(1)
    VNOISEDET = sqrt(
        PIXAPE * (float(NDIT) * RON * RON / PIXBINNED + DARKCUR * EXPTIME)
    )  # electrons
    print(f"VNOISEDET : {VNOISEDET}")
    SKYBCKAM = SKYBCK - (0.4 * (AM - 1))
    SIGMASKY = (10 ** ((16.85 - SKYBCKAM) / 2.5)) / RPOW  # ph cm-2 s-1 arcsec-2
    print(f"SIGMASKY : {SIGMASKY}")
    
    BBEXP = -14388.0 / (WL * TBCK)

    if BBEXP < -40.0:
        BBEXP = -40.0  # to avoid underflow

    SIGMATH = (1.4e12 * EBCK * exp(BBEXP)) / (WL**3 * RPOW)  # ph cm-2 s-1 arcsec-2
    print(f"SIGMATH : {SIGMATH}")
    EFFTEL = EFFT
    print(f"EFFTEL : {EFFTEL}")
    ALPHAP = ALPHA / 100.0
    AT = ALPHAP**AM
    EFFTELTOT = EFFTEL * AT
    print(f"EFFTELTOT : {EFFTELTOT}")
    EFF = EFFTELTOT * EFFI
    VNBCK = 0.7854 * EFF * ATEL * DAPE * DAPE * (SIGMASKY + SIGMATH)  # el/s
    VNOISEBCK = sqrt(VNBCK * EXPTIME * 3600)  # electrons
    print(f"VNOISEBCK : {VNOISEBCK}")

def printResults():
    """
    Function to print the results in a standard manner (valid for all situations SN, VMAG and SBMAG)
       Parameters:
                    None (variables comes from the global scope)
        Returns:
              None
    """

    # for SB results, original SLE output is commented
    print("%10.2f" % (DAPE))
    print("%10.2f" % (EFFTEL))
    print("%10.2f" % (AT))
    print("%10.1f" % (SKYBCKAM))
    print("%10.3f" % (SLE))
    print("%10.1f" % (DPIX))

    #      print('XANPIX = %10.6f'%(XANPIX))
    #      print('PIXBINNED = %10.3f'%(PIXBINNED))

    print("%10.1f" % (TOTPIXRE))
    print("%10.1f" % (RON))
    print("%10.1f" % (DARKCUR))


def printNoiseContribution():
    """
    Function to print the noise contribution (valid for all situations SN and VMAG)
       Parameters:
                    None (variables comes from the global scope)
        Returns:
              None
    """

    #     noise contribution

    VNOBJN = (
        SN**2 / 2 * (1.0 + sqrt(1 + 4 / SN**2 * (VNOISEBCK**2 + VNOISEDET**2)))
    )

    VNTOT_FRAC = VNOISEBCK**2 + VNOISEDET**2 + VNOBJN
    FRAC_NOISE_DET = VNOISEDET**2 / VNTOT_FRAC * 100.0

    print(
        "%s%6.1f%s"
        % ("Noise contribution from detector (RON+dark):", FRAC_NOISE_DET, " %")
    )

    FRAC_NOISE_BCK = VNOISEBCK**2 / VNTOT_FRAC * 100.0

    print(
        "%s%6.1f%s"
        % ("Noise contribution from background (sky+thermal):", FRAC_NOISE_BCK, " %")
    )

    FRAC_PHOTON_NOISE_OBJ = 100 - FRAC_NOISE_DET - FRAC_NOISE_BCK

    print(
        "%s%6.1f%s"
        % (
            "Noise contribution from photon noise of object:",
            FRAC_PHOTON_NOISE_OBJ,
            " %",
        )
    )


def printNoiseContributionSBMag():
    """
    Function to print the noise contribution specific for SBMAG since SN is computed differently (valid for all SBMAG mode only)
       Parameters:
                    None (variables comes from the global scope)
        Returns:
              None

    """

    #   noise contribution

    VNOBJN = (
        SNEX**2
        / 2
        * (1.0 + sqrt(1 + 4 / SNEX**2 * (VNOISEBCK**2 + VNOISEDET**2)))
    )
    VNTOT_FRAC = VNOISEBCK**2 + VNOISEDET**2 + VNOBJN
    
    FRAC_NOISE_DET = VNOISEDET**2 / VNTOT_FRAC * 100.0
    
    print(
        "%s%6.1f%s"
        % ("Noise contribution from detector (RON+dark):", FRAC_NOISE_DET, " %")
    )

    FRAC_NOISE_BCK = VNOISEBCK**2 / VNTOT_FRAC * 100.0

    print(
        "%s%6.1f%s"
        % ("Noise contribution from background (sky+thermal):", FRAC_NOISE_BCK, " %")
    )

    FRAC_PHOTON_NOISE_OBJ = 100 - FRAC_NOISE_DET - FRAC_NOISE_BCK

    print(
        "%s%6.1f%s"
        % (
            "Noise contribution from photon noise of object:",
            FRAC_PHOTON_NOISE_OBJ,
            " %",
        )
    )


def computeForSN():
    """
    Function to compute the values for SN mode
       Parameters:
                    None (variables comes from the global scope)
        Returns:
              None
    """

    # sample:
    #
    #  user@localhost$ python etc_hires_may2021.py -SN 12   -WL 1.65    -EX 1800

    global SKYBCK
    global EFFT
    global ALPHA

    def performComputation():
        #
        # Perform computation
        #

        global VLIMMAG
        global SBLIMMAG

        computeAuxiliaryValues()

        #     compact object

        VLIMMAG = 16.85 - 2.5 * log10(
            (RPOW * SN**2)
            / (2 * SLE * EFF * ATEL * 3600.0 * EXPTIME)
            * (1 + (sqrt(1 + 4 / SN**2 * (VNOISEBCK**2 + VNOISEDET**2))))
        )

        print(
            "%s%10.2f%s"
            % ("Limiting magnitude compact source:  ", VLIMMAG, "  AB units")
        )

        #     extended source

        SBLIMMAG = 2.5 * log10((3.141593 / 4) * DAPE**2) + VLIMMAG - 2.5 * log10(SLE)

        print(
            "%s%10.2f%s"
            % ("Limiting surface brightness extended source: ", SBLIMMAG, "  AB")
        )

        printNoiseContribution()
        printResults()
        exit(0)

    validadeSNParameter()
    validadeGeneralParameters()
    redefineParameters()
    SKYBCK = interpolateOnTable(SKYBCK, WL, TAB_SKY_WL, TAB_SKY_MAG)
    EFFT = interpolateOnTable(EFFT, WL, TAB_EFFT_WL, TAB_EFFT_VAL_0, 0.95)
    ALPHA = interpolateOnTable(ALPHA, WL, TAB_ALPHA_WL, TAB_ALPHA_VAL_0)
    interpolateOnSLETable()
    performComputation()
    exit(0)


def computeForVMAG():
    """
    Function to compute the values for VMAG mode
       Parameters:
                    None (variables comes from the global scope)
        Returns:
              None
    """

    # sample:
    #
    #  user@localhost$ python etc_hires_may2021.py -MA 12   -WL 1.65    -EX 1800

    global SKYBCK
    global EFFT
    global ALPHA

    def performComputation():
        #
        # Perform computation
        #

        global VNOBJ
        global SN

        computeAuxiliaryValues()

        VNOBJ = (SLE * EFF * ATEL * EXPTIME * 3600.0 / RPOW) * (
            10 ** ((16.85 - VMAG) / 2.5)
        )
        print(f"SLE : {SLE}")
        print(f"EFF : {EFF}")
        print(f"VNOBJ : {VNOBJ}")
        print(f"VNOISEBCK : {VNOISEBCK}")
        print(f"VNOISEDET : {VNOISEDET}")
        
        SN = sqrt((VNOBJ**2) / (VNOISEBCK**2 + VNOISEDET**2 + VNOBJ))

        print("%s%14.1f" % ("S/N ratio: ", SN))

        printNoiseContribution()
        printResults()

        exit(0)

    validadeVMAGParameter()
    validadeGeneralParameters()
    redefineParameters()
    SKYBCK = interpolateOnTable(SKYBCK, WL, TAB_SKY_WL, TAB_SKY_MAG)
    EFFT = interpolateOnTable(EFFT, WL, TAB_EFFT_WL, TAB_EFFT_VAL_0, 0.95)
    ALPHA = interpolateOnTable(ALPHA, WL, TAB_ALPHA_WL, TAB_ALPHA_VAL_0)
    interpolateOnSLETable()
    performComputation()
    exit(0)


def computeForSBMAG():
    """
    Function to compute the values for SBMAG mode
       Parameters:
                    None (variables comes from the global scope)
        Returns:
              None

    """

    # sample:
    #
    #  user@localhost$ python etc_hires_may2021.py -SB 12   -WL 1.65    -EX 1800

    global SKYBCK
    global EFFT
    global ALPHA

    def performComputation():
        #
        # Perform computation
        #

        global VMAGEX
        global SLEEX
        global SNEX
        global VNOBJEX

        computeAuxiliaryValues()

        VMAGEX = SBMAG - 2.5 * log10((3.141593 / 4) * (DAPE**2))
        SLEEX = 1.0
        VNOBJEX = (SLEEX * EFF * ATEL * EXPTIME * 3600.0 / RPOW) * (
            10 ** ((16.85 - VMAGEX) / 2.5)
        )
        SNEX = sqrt((VNOBJEX**2) / (VNOISEBCK**2 + VNOISEDET**2 + VNOBJEX))
        print(f"VNOBJEX {VNOBJEX}")
        print("%s%14.1f" % ("S/N ratio: ", SNEX))

        printNoiseContributionSBMag()
        printResults()
        exit(0)

    validadeSBMAGParameter()
    validadeGeneralParameters()
    redefineParameters()
    SKYBCK = interpolateOnTable(SKYBCK, WL, TAB_SKY_WL, TAB_SKY_MAG)
    EFFT = interpolateOnTable(EFFT, WL, TAB_EFFT_WL, TAB_EFFT_VAL_0, 0.95)
    ALPHA = interpolateOnTable(ALPHA, WL, TAB_ALPHA_WL, TAB_ALPHA_VAL_0)
    performComputation()
    exit(0)


# Tabular values for sky-backgroung

TAB_SKY_WL = np.array([0.36, 0.44, 0.55, 0.64, 0.80, 1.05, 1.25, 1.65, 2.60])
TAB_SKY_MAG = np.array([22.5, 22.5, 21.8, 21.5, 20.5, 20.5, 20.0, 19.5, 19.5])


# Tabular values for telescope efficiency (ESO)

TAB_EFFT_WL = np.linspace(0.35, 2.60, 46)
TAB_EFFT_VAL_0 = np.array(
    [
        0.10,
        0.299,
        0.469,
        0.565,
        0.61,
        0.642,
        0.674,
        0.699,
        0.712,
        0.713,
        0.715,
        0.757,
        0.788,
        0.803,
        0.812,
        0.82,
        0.826,
        0.834,
        0.842,
        0.848,
        0.855,
        0.86,
        0.865,
        0.87,
        0.873,
        0.876,
        0.878,
        0.88,
        0.88,
        0.881,
        0.881,
        0.88,
        0.882,
        0.882,
        0.883,
        0.883,
        0.884,
        0.884,
        0.884,
        0.884,
        0.884,
        0.885,
        0.885,
        0.886,
        0.886,
        0.886,
    ]
)


# Tabular values for alpha=e^-(tau(lambda)) (ESO)

TAB_ALPHA_WL = np.array(
    [0.35, 0.365, 0.44, 0.55, 0.70, 0.90, 1.00, 1.25, 1.5, 2.0, 2.2, 2.4, 2.6]
)

TAB_ALPHA_VAL_0 = np.array(
    [
        64.0,
        66.95,
        82.67,
        90.36,
        98.08,
        99.86,
        100.0,
        100.0,
        100.0,
        100.0,
        100.0,
        100.0,
        100.0,
    ]
)


SLE_WL = np.zeros(9)

TAB_EE_WL = np.array([0.36, 0.44, 0.55, 0.64, 0.80, 1.25, 1.65, 2.16, 3.50])

TAB_EE_00044 = np.array(
    [0.00229, 0.00263, 0.00282, 0.00325, 0.00380, 0.00480, 0.00474, 0.00646, 0.0142]
)

TAB_EE_002 = np.array(
    [0.0478, 0.0523, 0.0584, 0.0685, 0.0792, 0.0968, 0.116, 0.153, 0.290]
)

TAB_EE_01 = np.array([1.19, 1.30, 1.46, 1.67, 1.94, 2.42, 2.93, 3.76, 6.44])

TAB_EE_02 = np.array([4.66, 5.08, 5.68, 6.47, 7.46, 9.13, 11.0, 13.7, 20.2])

TAB_EE_04 = np.array([17.1, 18.4, 20.3, 22.7, 25.6, 30.1, 34.7, 40.1, 49.8])

TAB_EE_06 = np.array([33.5, 35.6, 38.5, 42.0, 46.1, 51.8, 56.8, 61.9, 69.5])

TAB_EE_08 = np.array([50.0, 52.4, 55.5, 59.1, 63.0, 68.0, 71.9, 75.6, 80.5])

TAB_EE_1 = np.array([64.0, 66.1, 68.8, 71.8, 74.8, 78.5, 81.2, 83.6, 86.7])

TAB_EE_16 = np.array([88.0, 88.3, 88.9, 89.8, 90.7, 91.8, 92.6, 93.3, 94.2])

TAB_EE_DAPE = np.array([0.0044, 0.02, 0.1, 0.2, 0.4, 0.6, 0.8, 1.0, 1.6])


# default values

WL = -1
EXPTIMES = -1
SN = -1
VMAG = -50
SBMAG = -50
NDIT = 1
RPOW = 1e5
DAPE = -1

SKYBCK = -1
TBCK = 283.0
EBCK = 0.2
DTEL = 38.5
COBS = 0.28
DPIX = -1
FCAM = 1.5
RON = -1

DARKCUR = -1
SLE = -1
EFFI = 0.1
EFFT = -1
AM = 1
ALPHA = -1
PIXBINNED = 1


# disctionary to process for globals

VAsDict = {
    "WL": "WL",
    "EX": "EXPTIMES",
    "SN": "SN",
    "MA": "VMAG",
    "SB": "SBMAG",
    "ND": "NDIT",
    "RP": "RPOW",
    "DA": "DAPE",
    "SK": "SKYBCK",
    "TB": "TBCK",
    "EB": "EBCK",
    "DT": "DTEL",
    "CO": "COBS",
    "DP": "DPIX",
    "FA": "FCAM",
    "RO": "RON",
    "DK": "DARKCUR",
    "SL": "SLE",
    "EI": "EFFI",
    "ET": "EFFT",
    "AM": "AM",
    "AL": "ALPHA",
    "PB": "PIXBINNED",
}


args = argv[1:]


# parse arguments

argDict = {x.split("=")[0].upper(): x.split("=")[1] for x in args}


# turn arguments into global variables
#
# TODO: This is VERY BAD practice. No technical issue, but makes the code ugly

for k in argDict:
    globals()[VAsDict[k]] = float(argDict[k])


# validade wether the user picked mode SN, VMAG of SBMAG

if compareFloat(SN, -1) and compareFloat(VMAG, -50) and compareFloat(SBMAG, -50):
    print("Must specify SN or Mag or Surface Brightness")
    exit(1)


# pick the mode and compute

if SN > 0:
    computeForSN()

if VMAG > -50:
    computeForVMAG()

if SBMAG > -50:
    computeForSBMAG()
