import pathlib

import numpy as np
import matplotlib.pyplot as plt
from joblib import Memory
from numpy import deg2rad
from numpy import sin, cos, tan, arcsin
from scipy.interpolate import interp1d
import math

def truncate(f, n):
    return math.floor(f * 10 ** n) / 10 ** n

def multiplyList(myList):
 
    # Multiply elements one by one
    result = 1
    for x in myList:
        result = result * x
    return result

def wave_order(blaze, gpmm, order=88, nwaves=10):
    """
    Gives an verctor sampling an order FSR for a given spectro
    inputs: 
        blaze : blaze angle in rad
        gpmm : nb of line per µm
        order: order that needs to be sambpled
        nwaves: number of wavelength to output
    outputs:
        waves: vector of wavelenght covering the order
    
    """
    c_m = 0.5
    lcen = 2*sin(blaze)/(order*gpmm)
    limin = lcen - c_m *lcen/ order
    limax = lcen + c_m * lcen / order
    return lcen,np.linspace(limin,limax,nwaves)

def wave_order_range(blaze, gpmm, order=88):
    """
    Gives an verctor sampling an order FSR for a given spectro
    inputs: 
        blaze : blaze angle in rad
        gpmm : nb of line per µm
        order: order that needs to be sambpled
        nwaves: number of wavelength to output
    outputs:
        blaze wavelenght, min and max wavelength in bandpass
    
    """
    c_m = 0.5
    lcen = 2*sin(blaze)/(order*gpmm)
    limin = lcen - c_m *lcen/ order
    limax = lcen + c_m * lcen / order
    return lcen

def max_efficiency(m_ge, p_ge, lcen):
    """
    Gives 
    
    """
    Eff_zero = m_ge*lcen+p_ge
    return Eff_zero

def calc_efficiency(alpha, blaze, order, wl, gpmm):
        aa= -sin(alpha) + order * wl *  gpmm
        if np.abs(aa) <=1. :
            bb =  arcsin(aa) 
        else:
            bb = 0.
        # blaze_wavelength = 2.*self.gpmm * sin(self.blaze) / order
        # fsr = blaze_wavelength / order

        x = (
                order
                * (cos(alpha) / cos(alpha - blaze))
                * (cos(blaze) - sin(blaze) / tan((alpha + bb) / 2.0) )
        )
        sinc = np.sinc(x)
       

        return sinc * sinc